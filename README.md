# Graphics

All Amy releated Grapics are stored here



![Logo](https://amy-assistant.gitlab.io/graphics/AMY-Logo.svg)
![Logo](https://amy-assistant.gitlab.io/graphics/AMY-SVG.svg)

![Logo](https://amy-assistant.gitlab.io/graphics/AMY-Animation.svg)

## StyleSheet

[Zeplin Project](https://zpl.io/2ZzomQq)


![Dark Stylesheet](https://amy-assistant.gitlab.io/graphics/Dark.svg)
![Light Stylesheet](https://amy-assistant.gitlab.io/graphics/Light.svg)

